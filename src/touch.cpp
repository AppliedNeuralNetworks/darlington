#include "touch.h"

namespace touch {

static MPR121 *mpr121;
static Thread touchthread;
static bool done = false;;
static EventFlags event_flags;

Touch::Touch(I2C *i2c, const uint8_t address) {
    mpr121 = new MPR121(i2c, address);

    done = false;
    touchthread.start(&touch::Touch::TouchThread);

    irq = new InterruptIn(MPR121_IRQ_PIN);
    irq->fall(touch::Touch::TouchTrigger);
}

Touch::~Touch() {
    done = true;
    event_flags.set(TOUCH_EVENT_OK);
    touchthread.join();
    delete mpr121; mpr121 = NULL;
}

void
Touch::TouchTrigger() {
    event_flags.set(TOUCH_EVENT_OK);
}

void
Touch::TouchThread() {
    while (!done) {
	event_flags.wait_any(TOUCH_EVENT_OK);
	printf("Touch status: %04x\n", mpr121->get());
    }
}

}; // namespace touch
