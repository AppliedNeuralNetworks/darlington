
#pragma once

// // == System ==
// #define SYSTEM_HEALTH_CHECK_PERIOD (1000ms)
// #define SERIAL_CONSOLE_BAUD_RATE (115200)

// // == Busses ==
//
// // Touch and IMU I2C (I2C4)
// #define I2C_4_CLOCK_PIN		PD_12
// #define I2C_4_DATA_PIN		PD_13
// #define MPR121_IRQ_PIN		PG_6
//
// // Display, SD Card, PSRAM, and MRAM (SPI4)
// #define DISPLAY_SPI_FREQ	(40000000ul)
// #define DISPLAY_MOSI_PIN	PE_14
// #define DISPLAY_MISO_PIN	PE_13
// #define DISPLAY_SCK_PIN		PE_12
// #define DISPLAY_CS_PIN		PE_11
// #define DISPLAY_DC_PIN		PF_3
// #define DISPLAY_SD_CS_PIN	PF_15
// #define DISPLAY_RESET_PIN	PE_8
//
// // Radio SPI Bus
// //#define RADIO_SPI_MOSI_PIN	PB_5
// //#define RADIO_SPI_MISO_PIN	PB_4
// //#define RADIO_SPI_SCK_PIN	PB_3
// //#define RADIO_SPI_CS_PIN	PA_15
// //#define RADIO_SPI_CE_PIN	PE_15
//
// #define GPS_TX_PIN		(PB_9)
// #define GPS_RX_PIN		(PB_8)
// #define GPS_INITIAL_BAUD	(115200)
//
// // == Peripherals ==
//
// // GPS
// // IO Expander
// //#define IO_EXPANDER_I2C_ADDR	(0x27)
//
// // Capacitive Touch Sensor
// //#define MPR121_IRQ_PIN		(PG_6)
//
// // == Lights ==
//
// // Device Illumination Lights
#define ARENA_LIGHT_1_PIN	(PG_6)
//
// // Perimeter Lights
// #define LIGHTS_DATA_PIN		(PG_2)
// #define LIGHTS_CLOCK_PIN    	(PG_3)
// #define LIGHTS_NUM_LEDS    	(14)

#define LIGHTS_CLOCK_PIN		(PG_5)
#define LIGHTS_DATA_PIN			(PG_6)
#define LIGHTS_NUM_LEDS			(19)

#define LIGHTS_INTENSITY_VERY_LOW       (3)
#define LIGHTS_INTENSITY_LOW            (10)
#define LIGHTS_INTENSITY_MEDIUM         (24)
#define LIGHTS_INTENSITY_MEDIUM_HIGH    (40)
#define LIGHTS_INTENSITY_HIGH           (100)
#define LIGHTS_INTENSITY_TURBO          (255)

#define LIGHTS_SIGNAL_FRONT_RIGHT       (0)
#define LIGHTS_HEADLIGHT_RIGHT_OUTER    (1)
#define LIGHTS_HEADLIGHT_RIGHT_INNER    (2)
#define LIGHTS_STATUS_FRONT             (3)
#define LIGHTS_HEADLIGHT_LEFT_INNER     (4)
#define LIGHTS_HEADLIGHT_LEFT_OUTER     (5)
#define LIGHTS_SIGNAL_FRONT_LEFT        (6)
#define LIGHTS_SIGNAL_REAR_LEFT         (7)
#define LIGHTS_BRAKELIGHT_LEFT_OUTER    (8)
#define LIGHTS_BRAKELIGHT_LEFT_INNER    (9)
#define LIGHTS_STATUS_REAR              (10)
#define LIGHTS_BRAKELIGHT_RIGHT_INNER   (11)
#define LIGHTS_BRAKELIGHT_RIGHT_OUTER   (12)
#define LIGHTS_SIGNAL_REAR_RIGHT        (13)

