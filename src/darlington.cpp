#include <FATFileSystem.h>
#include <SDBlockDevice.h>
#include <mbed.h>

//#include "mb85rs4mt.h"
#include "st7789v.hpp"
//#include "nrf24l01.h"
// #include "mt3339.h"
// #include "mpr121.h"
#include "lights.hpp"
#include "two_wire_rgb.hpp"
#include "darlington.hpp"

namespace appliedneural::darlington {

// Switch stdio baud rate. The name stdio here is not significant.
// Any reinitialization of USB serial hardware is sufficient to
// reconfigure parameters such as baud rate. Once instantiated,
// this object should persist indefinitely so that the USB serial
// is not shut down by the object's destructor.
#ifndef SERIAL_CONSOLE_BAUD_RATE
#define SERIAL_CONSOLE_BAUD_RATE (115200)
#endif
BufferedSerial stdio(USBTX, USBRX, SERIAL_CONSOLE_BAUD_RATE);

DigitalOut led(LED1);

// --- Busses ---
I2C *i2c_4 = nullptr;
SPI *display_spi = nullptr;
SPI *mram_radio_spi = nullptr;
BufferedSerial *uart_4 = nullptr;

// --- Peripherals ---
// ST7789V *display = NULL;
// MT3339 *gps = NULL;
// MPR121 *touch = NULL;

// --- Arena Lighting ---
//DigitalOut arena_light_1(PG_5);

// MB85RS4MT *fram = NULL;
// NRF24L01 *radio = NULL;
Lights *lights = nullptr;
SDBlockDevice *sd_card = nullptr;
FATFileSystem *file_system = nullptr;

Darlington::Darlington() {
    ThisThread::sleep_for(10ms);
    start_busses();
    start_peripherals();
}

Darlington::~Darlington() {
    stop_peripherals();
    stop_busses();
}

bool Darlington::run() {
    puts("\e[2H\e[2J\e[1m<[oO]> DARLINGTON ROBOT OS\e[0m\n");

    // Start services here
    // arena_light_1 = 1;

    puts("\n✅ System ready.\n");

    for (;;) {
	// TODO: some way to exit gracefully

	led = 1;
	// TODO: System health check
	ThisThread::sleep_for(5ms); // extra time so LED is visible
	led = 0;

	#ifndef SYSTEM_HEALTH_CHECK_PERIOD
	#define SYSTEM_HEALTH_CHECK_PERIOD (1000ms)
	#endif
	ThisThread::sleep_for(SYSTEM_HEALTH_CHECK_PERIOD);
    }

    // Stop services here

    return true;
}

bool Darlington::start_busses() {
    puts("\e[34m◇\e[0m I2C Bus for IMU and Touch Control (I2C_4)");
#   ifndef I2C_4_DATA_PIN
#       define I2C_4_DATA_PIN I2C_SDA
#   endif
#   ifndef I2C_4_CLOCK_PIN
#       define I2C_4_CLOCK_PIN I2C_SCL
#   endif
    i2c_4 = new I2C(I2C_4_DATA_PIN, I2C_4_CLOCK_PIN);
    i2c_4->frequency(100000);

    // puts("\e[34m◇\e[0m Serial Bus for GPS/SD");
    // uart_4 = new BufferedSerial(GPS_TX_PIN, GPS_RX_PIN, GPS_INITIAL_BAUD);
    //
    // if (!(display_spi = new SPI(DISPLAY_MOSI_PIN, DISPLAY_MISO_PIN,
    //     DISPLAY_SCK_PIN))) {
    // puts("Display/PSRAM/MRAM SPI failure");
    // return false;
    // }
    //
    // puts("\e[34m◇\e[0m SPI Bus for Display/SD");
    // if (!(display_spi = new SPI(DISPLAY_MOSI_PIN, DISPLAY_MISO_PIN,
    // 	    DISPLAY_SCK_PIN))) {
    // puts("MRAM/Radio SPI failure");
    // return false;
    // }
    // display_spi->frequency(DISPLAY_SPI_FREQ);

    // puts("\e[34m◇\e[0m SPI Bus for MRAM/Radio");
    // mram_radio_spi = new SPI(MRAM_RADIO_MOSI_PIN, MRAM_RADIO_MISO_PIN,
    // MRAM_RADIO_SCK_PIN);
    // if (!display_spi)
    // puts("Display SPI failure");
    // display_spi->frequency(MRAM_RADIO_SPI_FREQ);

    return true;
}

void Darlington::stop_busses() {
    DELETE_AND_NULL(i2c_4);
    DELETE_AND_NULL(mram_radio_spi);
    DELETE_AND_NULL(display_spi);
}

bool Darlington::start_peripherals() {
    // Lights (DotStar x 14)
    puts("\e[34m◇\e[0m Lights");
    lights = new Lights(LIGHTS_DATA_PIN, LIGHTS_CLOCK_PIN, LIGHTS_NUM_LEDS);
    lights->headlights_low();
    lights->gleam();
    // lights->parkinglights_on();
    // lights->braketap();
    // lights->headlights_low();

    //arena_light_1 = 1;

    // Mass Storage (SD card)
    // puts("\e[34m◇\e[0m Mass Storage");
    // sd_card = new SDBlockDevice(DISPLAY_MOSI_PIN, DISPLAY_MISO_PIN,
    //     DISPLAY_SCK_PIN, DISPLAY_SD_CS_PIN);
    // sd_card->init();
    // sd_card->frequency(DISPLAY_SPI_FREQ);
    // puts("  ∙ Filesystem");
    // file_system = new FATFileSystem("sd");
    // file_system->mount(sd_card);

    // Serial NV Storage (Fujitsu FeRAM)
    // puts("\e[34m◇\e[0m Non-volatile Storage");
    // fram = new MB85RS4MT(spi, SPI_CS_PIN);

    // Navigation (MediaTek GPS)
    // puts("\e[34m◇\e[0m GPS");
    // gps = new mt3339::MT3339(uart_4);
    // imu = ...

    // Communication (Nordik Radio Transceiver)
    // puts("\e[34m◇\e[0m Radio Transceiver");
    // radio = new NRF24L01(radio_spi, RADIO_SPI_CS_PIN, RADIO_SPI_CE_PIN);

    // Display (Sitronix ST7789V 320x240 LCD)
    // puts("\e[34m◇\e[0m Display");
    // display = new st7789v::ST7789V(display_spi, DISPLAY_CS_PIN,
    //     DISPLAY_DC_PIN);
    // if (!display)
    // puts("Display failed to initialize");
    //
    // puts("  ∙ Test Image");
    // char buf[2];
    // File testfile;
    // testfile.open(file_system, "/images/teapot.u16", O_RDONLY);
    // for (size_t i=0; i < sizeof(display->fb) / 2; i++) {
    // testfile.read(&buf, 2);
    // display->fb[i*2+1] = buf[0];
    // display->fb[i*2] = buf[1];
    // }
    // testfile.close();
    // display->send_frame();
    //
    // // Touch Control
    // touch = new MPR121(i2c_4);
    //
    // gps->start();

    return true;
}

void Darlington::stop_peripherals() {
    // DELETE_AND_NULL(file_system)
    // DELETE_AND_NULL(sd_card)
    // DELETE_AND_NULL(display)
    // DELETE_AND_NULL(lights)
}

}  // namespace appliedneural::darlington

