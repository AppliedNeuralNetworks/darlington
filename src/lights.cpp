
#include "lights.hpp"

#include "darlington.hpp"

namespace appliedneural::darlington {

static TwoWireRGB *two_wire_rgb = nullptr;
static EventFlags event_flags;

Lights::Lights(const PinName data_pin, const PinName clock_pin,
	       const size_t num_leds) {
    two_wire_rgb = new TwoWireRGB(data_pin, clock_pin, num_leds);

    gleam_thread.start(gleam_worker);
}

Lights::~Lights() {
    delete two_wire_rgb;
    two_wire_rgb = nullptr;
}

void Lights::headlights(uint8_t brightness) {
    two_wire_rgb->white(LIGHTS_HEADLIGHT_RIGHT_OUTER, brightness);
    two_wire_rgb->white(LIGHTS_HEADLIGHT_RIGHT_INNER, brightness);
    two_wire_rgb->white(LIGHTS_HEADLIGHT_LEFT_INNER, brightness);
    two_wire_rgb->white(LIGHTS_HEADLIGHT_LEFT_OUTER, brightness);
    two_wire_rgb->update();
}

void Lights::headlights_off() { headlights(0); }
void Lights::headlights_low() { headlights(LIGHTS_INTENSITY_LOW); }
void Lights::headlights_high() { headlights(LIGHTS_INTENSITY_HIGH); }
void Lights::headlights_turbo() { headlights(LIGHTS_INTENSITY_TURBO); }

void Lights::brakelights(uint8_t brightness) {
    two_wire_rgb->set(LIGHTS_BRAKELIGHT_LEFT_OUTER, brightness, 0, 0);
    two_wire_rgb->set(LIGHTS_BRAKELIGHT_LEFT_INNER, brightness, 0, 0);
    two_wire_rgb->set(LIGHTS_BRAKELIGHT_RIGHT_INNER, brightness, 0, 0);
    two_wire_rgb->set(LIGHTS_BRAKELIGHT_RIGHT_OUTER, brightness, 0, 0);
}

void Lights::brakelights_off() { brakelights(0); }
void Lights::brakelights_low() { brakelights(LIGHTS_INTENSITY_VERY_LOW); }
void Lights::brakelights_high() { brakelights(LIGHTS_INTENSITY_MEDIUM); }

void Lights::parkinglights(uint8_t red, uint8_t green, uint8_t blue) {
    two_wire_rgb->set(LIGHTS_SIGNAL_FRONT_RIGHT, red, green, blue);
    two_wire_rgb->set(LIGHTS_SIGNAL_FRONT_LEFT, red, green, blue);
    two_wire_rgb->set(LIGHTS_SIGNAL_REAR_LEFT, red, green, blue);
    two_wire_rgb->set(LIGHTS_SIGNAL_REAR_RIGHT, red, green, blue);
}

void Lights::parkinglights_off() { parkinglights(0, 0, 0); }

void Lights::parkinglights_on() { parkinglights(10, 2, 0); }

void Lights::signal_lights_left(uint8_t red, uint8_t green, uint8_t blue) {
    two_wire_rgb->set(LIGHTS_SIGNAL_FRONT_LEFT, red, green, blue);
    two_wire_rgb->set(LIGHTS_SIGNAL_REAR_LEFT, red, green, blue);
}

void Lights::signal_lights_right(uint8_t red, uint8_t green, uint8_t blue) {
    two_wire_rgb->set(LIGHTS_SIGNAL_FRONT_RIGHT, red, green, blue);
    two_wire_rgb->set(LIGHTS_SIGNAL_REAR_RIGHT, red, green, blue);
}

void Lights::signal_lights_left_low() { signal_lights_left(10, 2, 0); }

void Lights::signal_lights_left_high() { signal_lights_left(20, 8, 0); }

void Lights::signal_lights_right_low() { signal_lights_right(10, 2, 0); }

void Lights::signal_lights_right_high() { signal_lights_right(20, 8, 0); }

void Lights::gleam() { event_flags.set(EVENT_FLAG_GLEAM); }

void Lights::gleam_worker() {
    while (true) {
	event_flags.wait_any(EVENT_FLAG_GLEAM);
	for (uint8_t i = 0; i < 7; i++) {
	    switch (i) {
		case 0:
		    two_wire_rgb->set(3, 255, 0, 0);
		    two_wire_rgb->update();
		    break;
		case 1:
		    two_wire_rgb->set(3, 128, 128, 0);
		    two_wire_rgb->update();
		    break;
		case 2:
		    two_wire_rgb->set(3, 0, 255, 0);
		    two_wire_rgb->update();
		    break;
		case 3:
		    two_wire_rgb->set(3, 0, 128, 128);
		    two_wire_rgb->update();
		    break;
		case 4:
		    two_wire_rgb->set(3, 0, 0, 255);
		    two_wire_rgb->update();
		    break;
		case 5:
		    two_wire_rgb->set(3, 128, 0, 128);
		    two_wire_rgb->update();
		    break;
		case 6:
		    two_wire_rgb->off(3);
		    two_wire_rgb->update();
	    }
	    thread_sleep_for(50);
	}
    }
}

}  // namespace appliedneural::darlington
