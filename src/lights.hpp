#pragma once

#include <mbed.h>

#include "two_wire_rgb.hpp"

#define EVENT_FLAG_GLEAM (1)

namespace appliedneural::darlington {

class Lights {
   public:
    Lights(const PinName data_pin, const PinName clock_pin, const size_t num_leds);
    ~Lights();

    // Vehicle light operations
    void headlights_off();
    void headlights_low();
    void headlights_high();
    void headlights_turbo();

    void brakelights_off();
    void brakelights_low();
    void brakelights_high();
    void braketap();

    void parkinglights_off();
    void parkinglights_on();

    void signal_left();
    void signal_right();
    void signal_cancel();

    void hazard();
    void hazard_cancel();

    // Special functions
    void front_status(uint8_t red, uint8_t green, uint8_t blue);
    void front_status_off();
    void rear_status(uint8_t red, uint8_t green, uint8_t blue);
    void rear_status_off();

    // Effects
    void gleam();

   private:
    void parkinglights(uint8_t red, uint8_t green, uint8_t blue);
    void headlights(uint8_t brightness);
    void brakelights(uint8_t brightness);
    void signal_lights_left_off();
    void signal_lights_left_low();
    void signal_lights_left_high();
    void signal_lights_right_off();
    void signal_lights_right_low();
    void signal_lights_right_high();
    void signal_lights_left(uint8_t red, uint8_t green, uint8_t blue);
    void signal_lights_right(uint8_t red, uint8_t green, uint8_t blue);

    // Effects threads and machines
    Thread gleam_thread;
    static void gleam_worker();
};

}  // namespace appliedneural::darlington
