#pragma once

#include "darlington_conf.hpp"

#define DELETE_AND_NULL(ptr) \
    if (ptr) {               \
	delete ptr;          \
	ptr = NULL;          \
    }

namespace appliedneural::darlington {

class Darlington {
   public:
    Darlington();
    ~Darlington();
    bool run();

   private:
    bool start_busses();
    void stop_busses();
    bool start_peripherals();
    void stop_peripherals();

    bool shutdown = false;
};

}  // namespace appliedneural::darlington
