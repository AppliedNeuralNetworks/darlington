#pragma once
#include <mbed.h>
#include "mpr121.h"

#ifndef MPR121_IRQ_PIN
#define MPR121_IRQ_PIN (PG_6)
#endif

using namespace mpr121;

namespace touch {

enum touch_event_flags {
    TOUCH_EVENT_OK    = 1<<0, // Pin 0
    TOUCH_EVENT_BACK  = 1<<1, // Pin 1
    TOUCH_EVENT_LEFT  = 1<<2, // :
    TOUCH_EVENT_DOWN  = 1<<3, // .
    TOUCH_EVENT_UP    = 1<<4,
    TOUCH_EVENT_RIGHT = 1<<5,
};

class Touch {
    public:
	/* Object Lifecycle
	 *
	 *  i2c: Pointer to an mbed::I2C instance
	 *  addr: I2C address of device (default: 0x29)
	 */
	explicit Touch(I2C *i2c, const uint8_t address=MPR121_I2C_ADDRESS);
	~Touch();

	// Escapement
	static void TouchThread();
	static void TouchTrigger();

    private:
	InterruptIn *irq = NULL;
};

}; // namespace touch

