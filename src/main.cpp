#include "darlington.hpp"

int main() {
    appliedneural::darlington::Darlington robot;
    robot.run(); // Robot will run until it stops itself

    return 0;
}

