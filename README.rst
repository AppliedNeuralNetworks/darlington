
# Darlington Robot

## What is Darlington OS?

Darlington OS is an operating system for a small robot car called
"Darlingon." This project began with the ELEGOO Smart Robot Car V4.0 and
a small custom OS for the Uno and then the Mega, but the processsing
needs of the project have surpassed these microcontrollers.  In fact,
most of the original parts have changed, with the exception of the
chassis plates and the gearmotor/wheel assemblies.

## Hardware:

Currently, Darlington carries these components:

  - STM32 Nucleo H743ZI2 
  - TB6612 Motor Controller
  - 4 x right-angle 6V gearmotors with wheels
  - SparkFun uBlox ZED-F9R Dead Reckoning Module Breakout
  - 32GB SD Card
  - 2 x 7-pixel-long RGB DotStar strips
  - MakerFocus TFmini-s Time-of-flight Distance Sensor
  - Spektrum H3055 Ultra-speed Mid-Torque Sub-micro Digital Servo
  - Piezo transducer with impulse circuit to provide audible feedback
      for signal-light activity
  - 2 x 2Ah Lithium-polymer Batteries for Electronics and Motors separately.

## Required Libraries

This project aims for pure Mbed OS API compatibility to be compatible
with a wide range of products by Arm and ST.

Currently, no external libraries are used.

## Building

[to do]
