#pragma once
#include <mbed.h>

#include <chrono>

using namespace std::chrono;

namespace st7789v {

enum st7789v_commands {
    ST7789V_COMMAND_NOP = 0x00,
    ST7789V_COMMAND_SWRESET = 0x01,
    ST7789V_COMMAND_RDDID = 0x04,
    ST7789V_COMMAND_RDDST = 0x09,
    ST7789V_COMMAND_SLPIN = 0x10,
    ST7789V_COMMAND_SLPOUT = 0x11,
    ST7789V_COMMAND_PTLON = 0x12,
    ST7789V_COMMAND_NORON = 0x13,
    ST7789V_COMMAND_INVOFF = 0x20,
    ST7789V_COMMAND_INVON = 0x21,
    ST7789V_COMMAND_DISPOFF = 0x28,
    ST7789V_COMMAND_DISPON = 0x29,
    ST7789V_COMMAND_CASET = 0x2a,
    ST7789V_COMMAND_RASET = 0x2b,
    ST7789V_COMMAND_RAMWR = 0x2c,
    ST7789V_COMMAND_RAMRD = 0x2e,
    ST7789V_COMMAND_PTLAR = 0x30,
    ST7789V_COMMAND_TEOFF = 0x34,
    ST7789V_COMMAND_TEON = 0x35,
    ST7789V_COMMAND_MADCTL = 0x36,
    ST7789V_COMMAND_COLMOD = 0x3a,
    ST7789V_COMMAND_FRMCTR1 = 0xb1,
    ST7789V_COMMAND_FRMCTR2 = 0xb2,
    ST7789V_COMMAND_FRMCTR3 = 0xb3,
    ST7789V_COMMAND_INVCTR = 0xb4,
    ST7789V_COMMAND_DISSET5 = 0xb6,
    ST7789V_COMMAND_GCTRL = 0xb7,
    ST7789V_COMMAND_GTADJ = 0xb8,
    ST7789V_COMMAND_VCOMS = 0xbb,
    ST7789V_COMMAND_LCMCTRL = 0xc0,
    ST7789V_COMMAND_IDSET = 0xc1,
    ST7789V_COMMAND_VDVVRHEN = 0xc2,
    ST7789V_COMMAND_VRHS = 0xc3,
    ST7789V_COMMAND_VDVS = 0xc4,
    ST7789V_COMMAND_VMCTR1 = 0xc5,
    ST7789V_COMMAND_FRCTRL2 = 0xc6,
    ST7789V_COMMAND_CABCCTRL = 0xc7,
    ST7789V_COMMAND_PWCTRL1 = 0xd0,
    ST7789V_COMMAND_RDID1 = 0xda,
    ST7789V_COMMAND_RDID2 = 0xdb,
    ST7789V_COMMAND_RDID3 = 0xdc,
    ST7789V_COMMAND_RDID4 = 0xdd,
    ST7789V_COMMAND_GMCTRP1 = 0xe0,
    ST7789V_COMMAND_GMCTRN1 = 0xe1,
    ST7789V_COMMAND_PWCTR6 = 0xfc
};

enum st7789v_mdctl {
    ST7789V_COMMAND_MADCTL_MH = 0x04,
    ST7789V_COMMAND_MADCTL_RGB = 0x08,
    ST7789V_COMMAND_MADCTL_ML = 0x10,
    ST7789V_COMMAND_MADCTL_MV = 0x20,
    ST7789V_COMMAND_MADCTL_MX = 0x40,
    ST7789V_COMMAND_MADCTL_MY = 0x80,
};

enum st7789v_colors {
    ST7789V_COLOR_BLACK = 0x0000,
    ST7789V_COLOR_BLUE = 0x001F,
    ST7789V_COLOR_CYAN = 0x07FF,
    ST7789V_COLOR_GREEN = 0x07E0,
    ST7789V_COLOR_MAGENTA = 0xF81F,
    ST7789V_COLOR_ORANGE = 0xFC00,
    ST7789V_COLOR_RED = 0xF800,
    ST7789V_COLOR_WHITE = 0xFFFF,
    ST7789V_COLOR_YELLOW = 0xFFE0
};

enum st7789v_colormodes { ST7789V_COLORMODE_16BIT = 0x55 };

#ifndef ST7789V_ROWS
#define ST7789V_ROWS (240)
#endif

#ifndef ST7789V_COLS
#define ST7789V_COLS (320)
#endif

class ST7789V {
   public:
    /* Object Lifecycle
     *
     *  spi: Pointer to an mbed::SPI instance. Harmless if SPI was
     *    instantiated with an SSEL pin, since exclusive access here
     *    uses lock() instead of select(), and does its own chip
     *    select by explicitly toggling the pin named by the cs_pin
     *    parameter, below.
     *  cs_pin: mbed::PinName of pin connected to the peripheral's
     *    chip select pin
     *  dc_pin: mbed::PinName of pin connected to the peripheral's
     *    data/command pin
     */
    ST7789V(SPI *spi, const PinName cs_pin, const PinName dc_pin);
    ~ST7789V();

    void reset(void);
    void clear(const uint16_t color = 0x00);
    void sleep(const bool on = true);
    void enable(const bool enable = true);

    void inverse(const bool on = true);
    void normal_partial(const bool normal = true);
    void colormode(const char mode);
    void memory_address_control(const char madctl);

    void send_frame(void);
    void blt(const uint16_t **source, uint16_t **dest, const size_t source_row,
	     const size_t source_col, const size_t dest_row,
	     const size_t dest_col, const size_t rows, const size_t cols);

    // Frame buffer
    char fb[ST7789V_ROWS * ST7789V_COLS * 2];
    SPI *spi = NULL;

   private:
    // Args
    PinName cs_pin = NC;
    PinName dc_pin = NC;

    // I/O
    DigitalOut *gpio_chip_select = NULL;
    DigitalOut *gpio_data_command = NULL;

    void spi_send_command(const int command, const milliseconds wait = 0ms,
			  const size_t data_len = 0, ...);
    void spi_command_mode(void);
    void spi_data_mode(void);
    void spi_select(void);
    void spi_deselect(void);
};

}  // namespace st7789v
