#include <hal/dma_api.h>
#include "st7789v.hpp"

namespace st7789v {

ST7789V::ST7789V(SPI *spi, const PinName cs_pin, const PinName dc_pin) {
    // Save args
    this->spi = spi;
    this->cs_pin = cs_pin;
    this->dc_pin = dc_pin;

    // Start GPIOs
    gpio_chip_select = new DigitalOut(cs_pin, 1);   // 0: select, 1: release
    gpio_data_command = new DigitalOut(dc_pin, 0);  // 0: command, 1: data

    sleep(false);
    inverse(false);
    spi_send_command(ST7789V_COMMAND_COLMOD, 0ms, 1, ST7789V_COLORMODE_16BIT);
    memory_address_control(ST7789V_COMMAND_MADCTL_MV |
	    ST7789V_COMMAND_MADCTL_MY); // Swap X/Y and flip horizontally
    spi_send_command(ST7789V_COMMAND_RASET, 0ms, 4,
	    0, 0, ST7789V_ROWS>>8, ST7789V_ROWS&0xff); // Redimension for
    spi_send_command(ST7789V_COMMAND_CASET, 0ms, 4,    // swapped height/width
	    0, 0, ST7789V_COLS>>8, ST7789V_COLS&0xff);

    clear();
    send_frame();

    enable(true);
}

ST7789V::~ST7789V() {
    gpio_chip_select->write(1);
    delete gpio_chip_select; gpio_chip_select = NULL;
    gpio_data_command->write(0);
    delete gpio_data_command; gpio_data_command = NULL;
}

void stupid_callback(int bar) {}

void ST7789V::send_frame() {
    spi_select();
    spi_command_mode();
    spi->write(0x2c);
    spi_data_mode();
    spi->write(fb, sizeof(fb), NULL, 0);

    //char *f_b = (char *)malloc(153600);
    //for (size_t i=0; i<sizeof(fb); i++)
	//f_b[i] = fb[i];
    ////memcpy(f_b, fb, sizeof(fb));
    //spi->transfer(f_b, sizeof(f_b), (char *)NULL, 8, callback(stupid_callback));

    spi_command_mode();
    spi->write(0);
    spi_deselect();
}

//void ST7789V::complete_cb(Buffer tx_buffer, Buffer rx_buffer, int narg) { }

void ST7789V::clear(const uint16_t color) {
    for (size_t i=0; i < sizeof(fb); i++)
	fb[i] = 0;
}

void
ST7789V::blt(const uint16_t **source, uint16_t **dest, 
	const size_t source_row, const size_t source_col,
	const size_t dest_row, const size_t dest_col,
	const size_t rows, const size_t cols) {
    for (size_t r=0; r<rows; r++) {
	for (size_t c=0; c<cols; c++)
	    dest[dest_row+r][dest_col+c] = source[source_row+r][source_col+c];
    }
}

// API-like Calls
void 
ST7789V::reset() {
    spi_send_command(ST7789V_COMMAND_SWRESET, 150ms);
}

void
ST7789V::memory_address_control(const char madctl) {
    spi_send_command(ST7789V_COMMAND_MADCTL, 0ms, 1, madctl);
}

void
ST7789V::enable(const bool enable) {
    spi_send_command(
	    enable ? ST7789V_COMMAND_DISPON : ST7789V_COMMAND_DISPOFF, 0ms);
}

void
ST7789V::inverse(const bool on) {
    spi_send_command(on ? ST7789V_COMMAND_INVOFF : ST7789V_COMMAND_INVON, 0ms);
}

void
ST7789V::colormode(const char mode) {
    spi_send_command(ST7789V_COMMAND_COLMOD, 0ms, 1, 0x55);
}

void
ST7789V::normal_partial(const bool normal) {
    spi_send_command(
	    normal ? ST7789V_COMMAND_NORON : ST7789V_COMMAND_PTLON, 10ms);
}

void
ST7789V::sleep(const bool on) {
    spi_send_command(
	    on ? ST7789V_COMMAND_SLPIN : ST7789V_COMMAND_SLPOUT, 120ms);
}


// Low-level SPI Communication
void
ST7789V::spi_send_command(const int command, const milliseconds wait,
	const size_t data_len, ...) {
    spi_select();
    spi_command_mode();
    spi->write(command);

    if (data_len) {
	spi_data_mode();
	va_list args;
	va_start(args, data_len);
	for (size_t i=0; i<data_len; i++)
	    spi->write(va_arg(args, int));
	va_end(args);
	spi_command_mode();
    }
    if (wait > 0ms)
	ThisThread::sleep_for(wait);
    spi_deselect();
}

void
ST7789V::spi_command_mode() {
    gpio_data_command->write(0);
}

void
ST7789V::spi_data_mode() {
    gpio_data_command->write(1);
}

void
ST7789V::spi_select() {
    spi->lock();
    gpio_chip_select->write(0);
}

void
ST7789V::spi_deselect() {
    gpio_chip_select->write(1);
    spi->unlock();
}

}; // namespace st7789v
