
#pragma once

#include <mbed.h>

 
enum mr25h40df_opcodes {
  MR25H40DF_OPCODE_WREN	    = 0x06,   // Set write-enable latch
  MR25H40DF_OPCODE_WRDI	    = 0x04,   // Reset write-enable latch
  MR25H40DF_OPCODE_RDSR	    = 0x05,   // Read status register
  MR25H40DF_OPCODE_WRSR	    = 0x01,   // Write status register
  MR25H40DF_OPCODE_READ	    = 0x03,   // Read memory code
  MR25H40DF_OPCODE_WRITE    = 0x02,   // Write Memory code
  MR25H40DF_OPCODE_SLEEP    = 0xb9,   // Sleep mode
  MR25H40DF_OPCODE_WAKE     = 0xab,   // Wave up
};

class MR25H40DF {
    public:
	/* Constructor
	 *  Enables write on construction
	 *
	 *  spi: Pointer to an mbed::SPI instance. Harmless if SPI was instantiated with an SSEL pin,
	 *	since exclusive access here uses lock() instead of select(), and does its own chip
	 *	select by explitely toggling the pin named by the cs_pin parameter, below.
	 *  cs_pin: mbed::PinName of pin connected to the peripheral's chip select pin
	 */
	MR25H40DF(SPI* spi, const PinName cs_pin);

	/* Destructor
	 *  Disables write on destruction
	 */
	~MR25H40DF();

	/* Enable writing to chip */
	void write_enable();

	/* Disable writing to chip */
	void write_disable();

	/* Read the status register
	 *
	 *  Returns the status register as a single byte
	 */
	char read_status_register();

	/* Writes a single byte into the status register */
	void write_status_register(const char value);

	/* Write a single byte to a memory location
	 *
	 *  addr: memory location, 13 most significant bits are ignored
	 */
	char read(const uint32_t addr);

	/* Read multiple bytes from a series of memory locations
	 *
	 *  addr: memory location, 13 most significant bits are ignored
	 *  values: pointer to an existing array, long enough to contain len bytes
	 *  len: number of bytes to read
	 *
	 *  NOTE: This routine obtains the bytes from the chip one at a time
	 *  in separate transactions. This is because this chip does not
	 *  support auto-increment when a stream of bytes is read unless
	 *  special signaling is done using the CS line. This also
	 *  introduces an ambiguity into the protocol which can
	 *  cause erroneous data on sequential reads, and corruption on
	 *  sequential writes.
	 */
	void read(const uint32_t addr, char *values, size_t len);

	/* Write a single byte to a memory location
	 *
	 *  addr: memory location, 13 most significant bits are ignored
	 *  value: byte to be written
	 */
	void write(const uint32_t addr, const char value);

	/* Write multiple bytes to a series of memory locations
	 *
	 *  addr: memory location, 13 most significant bits are ignored
	 *  values: pointer to an existing array of bytes
	 *  len: number of bytes to write
	 *
	 *  NOTE: This routine writes the bytes to the chip one at a time
	 *  in separate transactions. This is because this chip does not
	 *  support auto-increment when a stream of bytes is written unless
	 *  special signaling is done using the CS line. This also
	 *  introduces an ambiguity into the protocol which can
	 *  cause erroneous data on sequential reads, and corruption on
	 *  sequential writes.
	 */
	void write(const uint32_t addr, const char *values, const size_t len);

	/* Fill all locations in a region of memory with a byte value
	 *
	 *  addr: memory location, 13 most significant bits are ignored
	 *  value: byte value to be written
	 *  len: number of memory locations to write the byte into
	 */
	void fill(const uint32_t addr, const char value, size_t len);

	/* Zero all locations in a region of memory
	 *
	 *  addr: memory location, 13 most significant bits are ignored
	 *  len: number of memory locations to zero
	 */
	void zero(const uint32_t addr, size_t len);

    private:
	bool initialized = false;
        SPI* spi = NULL;
        DigitalOut* chip_select = NULL;

	void select();
	void deselect();
	void insert_cmd_addr(char *buf, const char cmd, const uint32_t addr);
	void insert_cmd_addr_value(char *buf, const char cmd, const uint32_t addr, const char value);
	void tx_cmd(const char cmd);
	char tx_get_value(const char *buf, const size_t len);
	void txrx(const char *tx_buf, const size_t tx_len, char *rx_buf, const size_t rx_len);
};

