#include "mr25h40df.h"

MR25H40DF::MR25H40DF(SPI* spi, const PinName cs_pin) {
    this->spi = spi;
    this->chip_select = new DigitalOut(cs_pin, 1);

    puts("✅ FeRAM device online: 512kB");

    write_enable();
}

MR25H40DF::~MR25H40DF() {
    write_disable();
}


void MR25H40DF::write_enable() {
    tx_cmd(MR25H40DF_OPCODE_WREN);
}

void MR25H40DF::write_disable() {
    tx_cmd(MR25H40DF_OPCODE_WRDI);
}

char MR25H40DF::read_status_register() {
    char buf[1] = {MR25H40DF_OPCODE_RDSR};
    return tx_get_value(buf, 1);
}

void MR25H40DF::write_status_register(const char value) {
    char buf[2] = {MR25H40DF_OPCODE_WRSR, value};
    txrx(buf, 2, NULL, 0);
}

char MR25H40DF::read(const uint32_t addr) {
    char buf[4] = {0};
    insert_cmd_addr(buf, MR25H40DF_OPCODE_READ, addr);
    return tx_get_value(buf, 4);
}

void MR25H40DF::read(const uint32_t addr, char *values, const size_t len) {
    char buf[4] = {0};
    spi->lock();
    for (uint32_t i = 0; i < len; i++) {
	select();
	insert_cmd_addr(buf, MR25H40DF_OPCODE_READ, addr + i);
	this->spi->write(buf, 4, NULL, 0);
	this->spi->write(NULL, 0, values + i, 1);
	deselect();
    }
    spi->unlock();
}

void MR25H40DF::write(const uint32_t addr, const char value) {
    char buf[5];
    insert_cmd_addr_value(buf, MR25H40DF_OPCODE_WRITE, addr, value);
    txrx(buf, 5, NULL, 0);
}

void MR25H40DF::write(const uint32_t addr, const char *values, const size_t len) {
    char buf[5];
    spi->lock();
    for (uint32_t i = 0; i < len; i++) {
	insert_cmd_addr(buf, MR25H40DF_OPCODE_WRITE, addr + i);
	buf[4] = values[i];
	select();
	this->spi->write(buf, 5, NULL, 0);
	deselect();
    }
    spi->unlock();
}

void MR25H40DF::fill(const uint32_t addr, const char value, size_t len) {
    char buf[5];
    insert_cmd_addr(buf, MR25H40DF_OPCODE_WRITE, addr);
    spi->lock();
    select();
    this->spi->write(buf, 5, NULL, 0);
    for (uint32_t i = 0; i < len; i++) {
	this->spi->write(&value, 1, NULL, 0);
    }
    deselect();
    spi->unlock();
}

void MR25H40DF::zero(const uint32_t addr, size_t len) {
    fill(addr, len, 0);
}


void MR25H40DF::select() {
    *chip_select = 0;
}

void MR25H40DF::deselect() {
    *chip_select = 1;
}

void MR25H40DF::insert_cmd_addr(char *buf, const char cmd, const uint32_t addr) {
    buf[0] = cmd;
    buf[1] = (char)(addr >> 16 & 0xff);
    buf[2] = (char)(addr >>  8 & 0xff);
    buf[3] = (char)(addr       & 0xff);
}

void MR25H40DF::insert_cmd_addr_value(char *buf, const char cmd, const uint32_t addr, const char value) {
    insert_cmd_addr(buf, cmd, addr);
    buf[4] = value;
}

void MR25H40DF::tx_cmd(const char cmd) {
    char buf[1] = {cmd};
    txrx(buf, 1, NULL, 0);
}

char MR25H40DF::tx_get_value(const char *buf, const size_t len) {
    char value = 0;
    txrx(buf, len, &value, 1);
    return value;
}

void MR25H40DF::txrx(const char *tx_buf, const size_t tx_len, char *rx_buf, const size_t rx_len) {
    spi->lock();
    select();
    if (tx_len)
	this->spi->write(tx_buf, tx_len, NULL, 0);
    if (rx_len)
	this->spi->write(NULL, 0, rx_buf, rx_len);
    deselect();
    spi->unlock();
}

