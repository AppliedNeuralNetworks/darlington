#include "nrf24l01.h"

NRF24L01::NRF24L01(SPI* spi, const PinName cs_pin, const PinName ce_pin) {
    this->spi = spi;
    this->chip_select = new DigitalOut(cs_pin);
    this->chip_enable = new DigitalOut(ce_pin);

    spi->lock();
    *chip_select = 0;
    
    *chip_select = 1;
    spi->unlock();
}

char MB85RS4MT::read(const uint32_t addr) {
    char buf[4] = {0};
    insert_cmd_addr(buf, MB85RS4MT_OPCODE_READ, addr);
    return tx_get_value(buf, 4);
}

void MB85RS4MT::read(const uint32_t addr, char *values, const size_t len) {
    char buf[4] = {0};
    spi->lock();
    for (uint32_t i = 0; i < len; i++) {
	select();
	insert_cmd_addr(buf, MB85RS4MT_OPCODE_READ, addr + i);
	this->spi->write(buf, 4, NULL, 0);
	this->spi->write(NULL, 0, values + i, 1);
	deselect();
    }
    spi->unlock();
}

void MB85RS4MT::write(const uint32_t addr, const char value) {
    char buf[5];
    insert_cmd_addr_value(buf, MB85RS4MT_OPCODE_WRITE, addr, value);
    txrx(buf, 5, NULL, 0);
}

void MB85RS4MT::write(const uint32_t addr, const char *values, const size_t len) {
    char buf[5];
    spi->lock();
    for (uint32_t i = 0; i < len; i++) {
	insert_cmd_addr(buf, MB85RS4MT_OPCODE_WRITE, addr + i);
	buf[4] = values[i];
	select();
	this->spi->write(buf, 5, NULL, 0);
	deselect();
    }
    spi->unlock();
}

void MB85RS4MT::select() {
    *chip_select = 0;
}

void MB85RS4MT::deselect() {
    *chip_select = 1;
}

