
#include <mbed.h>

#define NRF24L01_OP_READ_REGISTER   (0b00000000)
#define NRF24L01_OP_WRITE_REGISTER  (0b00100000)
#define NRF24L01_OP_R_RX_PAYLOAD    (0b01100001)
#define NRF24L01_OP_W_TX_PAYLOAD    (0b10100000)
#define NRF24L01_OP_FLUSH_TX	    (0b11100001)
#define NRF24L01_OP_FLUSH_RX	    (0b11100010)
#define NRF24L01_OP_REUSE_TX_PL	    (0b11100011)
#define NRF24L01_OP_NOOP	    (0b11111111)

#define NRF24L01_REG_CONFIG	    (0x00)
#define NRF24L01_REG_EN_AA	    (0x01)
#define NRF24L01_REG_EN_RXADDR	    (0x02)
#define NRF24L01_REG_SETUP_AW	    (0x03)
#define NRF24L01_REG_SETUP_RETR	    (0x04)
#define NRF24L01_REG_RF_CH	    (0x05)
#define NRF24L01_REG_RF_SETUP	    (0x06)
#define NRF24L01_REG_STATUS	    (0x07)
#define NRF24L01_REG_OBSERVE_TX	    (0x08)
#define NRF24L01_REG_CD		    (0x09)
#define NRF24L01_REG_RX_ADDR_P0	    (0x0A)
#define NRF24L01_REG_RX_ADDR_P1	    (0x0B)
#define NRF24L01_REG_RX_ADDR_P2	    (0x0C)
#define NRF24L01_REG_RX_ADDR_P3	    (0x0D)
#define NRF24L01_REG_RX_ADDR_P4	    (0x0E)
#define NRF24L01_REG_RX_ADDR_P5	    (0x0F)
#define NRF24L01_REG_TX_ADDR	    (0x10)
#define NRF24L01_REG_RX_PW_P0	    (0x11)
#define NRF24L01_REG_RX_PW_P1	    (0x12)
#define NRF24L01_REG_RX_PW_P2	    (0x13)
#define NRF24L01_REG_RX_PW_P3	    (0x14)
#define NRF24L01_REG_RX_PW_P4	    (0x15)
#define NRF24L01_REG_RX_PW_P5	    (0x16)
#define NRF24L01_REG_FIFO_STATUS    (0x17)
#define NRF24L01_REG_TX_PLD	    (0x1C)
#define NRF24L01_REG_RX_PLD	    (0x1D)

class NRF24L01 {
    public:
	NRF24L01(SPI* spi, const PinName cs_pin, const PinName ce_pin);
        //void send(const uint8_t reg, const uint8_t val);

    private:
	

        SPI* spi = NULL;
	DigitalOut* chip_select = NULL;
	DigitalOut* chip_enable = NULL;
};
