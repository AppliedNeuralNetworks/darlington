#pragma once

#include <mbed.h>

typedef struct {
    uint8_t brightness;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Pixel;

class TwoWireRGB {
   public:
    TwoWireRGB(const PinName data_pin, const PinName clock_pin,
	       const size_t num_leds);
    ~TwoWireRGB();

    void set(size_t pixel_num, uint8_t red, uint8_t green, uint8_t blue);
    void set(uint8_t red, uint8_t green, uint8_t blue);
    void white(size_t pixel_num, uint8_t brightness);
    void white(uint8_t brightness);
    void off(size_t pixel_num);
    void off();

    void update();

   private:
    void send_byte(uint8_t value);
    void send_pixel(Pixel *pixel);

    PinName data_pin, clock_pin;
    size_t num_leds;

    DigitalOut *data_out, *clock_out;
    Pixel *pixels;
};
