
#include <two_wire_rgb.hpp>

TwoWireRGB::TwoWireRGB(const PinName data_pin, const PinName clock_pin,
		       const size_t num_leds) {
    this->data_pin = data_pin;
    this->clock_pin = clock_pin;
    this->num_leds = num_leds;

    data_out = new DigitalOut(data_pin, 0);
    clock_out = new DigitalOut(clock_pin, 0);

    pixels = (Pixel *)calloc(num_leds, sizeof(Pixel));
    update();
}

TwoWireRGB::~TwoWireRGB() {
    free(pixels);
    pixels = NULL;
}

void TwoWireRGB::update() {
    // pixels[0].red=0xff;
    //
    // pixels[1].red=0x7f;
    // pixels[1].green=0x7f;
    //
    // pixels[2].green=0xff;
    //
    // pixels[3].green=0x7f;
    // pixels[3].blue=0x7f;
    //
    // pixels[4].blue=0xff;
    //
    // pixels[5].red=0x7f;
    // pixels[5].blue=0x7f;

    for (int i = 0; i < 4; i++) send_byte(0);
    for (size_t i = 0; i < num_leds; i++) send_pixel(&pixels[i]);
    for (int i = 0; i < 4; i++) send_byte(0x0);
    clock_out->write(0);
    data_out->write(0);
}

void TwoWireRGB::set(size_t pixel_num, uint8_t red, uint8_t green,
		     uint8_t blue) {
    pixels[pixel_num].red = red;
    pixels[pixel_num].green = green;
    pixels[pixel_num].blue = blue;
}

void TwoWireRGB::set(uint8_t red, uint8_t green, uint8_t blue) {
    for (size_t i = 0; i < num_leds; i++) set(i, red, green, blue);
}

void TwoWireRGB::white(size_t pixel_num, uint8_t brightness) {
    set(pixel_num, brightness, brightness, brightness);
}

void TwoWireRGB::white(uint8_t brightness) {
    set(brightness, brightness, brightness);
}

void TwoWireRGB::off(size_t pixel_num) { set(pixel_num, 0, 0, 0); }

void TwoWireRGB::off() {
    for (size_t i = 0; i < num_leds; i++) off(i);
}

void TwoWireRGB::send_pixel(Pixel *pixel) {
    send_byte(0xe7);
    send_byte(pixel->blue);
    send_byte(pixel->green);
    send_byte(pixel->red);
}

void TwoWireRGB::send_byte(uint8_t value) {
    for (int i = 7; i >= 0; i--) {
	data_out->write(value >> i & 1);
	clock_out->write(1);
	clock_out->write(0);
    }
}

