#include "mb85rs4mt.h"

MB85RS4MT::MB85RS4MT(SPI* spi, const PinName cs_pin) {
    this->spi = spi;
    this->chip_select = new DigitalOut(cs_pin, 1);

    // Verify the chip ID
    mb85rs4mt_id_t id;
    read_id(&id);
    if ((id.manufacturer != 0x04) or
	    (id.continuation != 0x7f) or
	    (id.product_id_1 != 0x49) or
	    (id.product_id_2 != 0x03)) {
	puts("❌ FeRAM device returned incorrect ID values");
	return;
    }
    puts("✅ FeRAM device online: 512kB");

    write_enable();
}

MB85RS4MT::~MB85RS4MT() {
    write_disable();
}


void MB85RS4MT::read_id(mb85rs4mt_id_t *id) {
    char tx_buf[1] = {MB85RS4MT_OPCODE_RDID};
    char rx_buf[4] = {0};
    txrx(tx_buf, 1, rx_buf, 4);
    id->manufacturer = rx_buf[0];
    id->continuation = rx_buf[1];
    id->product_id_1 = rx_buf[2];
    id->product_id_2 = rx_buf[3];
}

void MB85RS4MT::write_enable() {
    tx_cmd(MB85RS4MT_OPCODE_WREN);
}

void MB85RS4MT::write_disable() {
    tx_cmd(MB85RS4MT_OPCODE_WRDI);
}

char MB85RS4MT::read_status_register() {
    char buf[1] = {MB85RS4MT_OPCODE_RDSR};
    return tx_get_value(buf, 1);
}

void MB85RS4MT::write_status_register(const char value) {
    char buf[2] = {MB85RS4MT_OPCODE_WRSR, value};
    txrx(buf, 2, NULL, 0);
}

char MB85RS4MT::read(const uint32_t addr) {
    char buf[4] = {0};
    insert_cmd_addr(buf, MB85RS4MT_OPCODE_READ, addr);
    return tx_get_value(buf, 4);
}

void MB85RS4MT::read(const uint32_t addr, char *values, const size_t len) {
    char buf[4] = {0};
    spi->lock();
    for (uint32_t i = 0; i < len; i++) {
	select();
	insert_cmd_addr(buf, MB85RS4MT_OPCODE_READ, addr + i);
	this->spi->write(buf, 4, NULL, 0);
	this->spi->write(NULL, 0, values + i, 1);
	deselect();
    }
    spi->unlock();
}

void MB85RS4MT::write(const uint32_t addr, const char value) {
    char buf[5];
    insert_cmd_addr_value(buf, MB85RS4MT_OPCODE_WRITE, addr, value);
    txrx(buf, 5, NULL, 0);
}

void MB85RS4MT::write(const uint32_t addr, const char *values, const size_t len) {
    char buf[5];
    spi->lock();
    for (uint32_t i = 0; i < len; i++) {
	insert_cmd_addr(buf, MB85RS4MT_OPCODE_WRITE, addr + i);
	buf[4] = values[i];
	select();
	this->spi->write(buf, 5, NULL, 0);
	deselect();
    }
    spi->unlock();
}

void MB85RS4MT::fill(const uint32_t addr, const char value, size_t len) {
    char buf[5];
    insert_cmd_addr(buf, MB85RS4MT_OPCODE_WRITE, addr);
    spi->lock();
    select();
    this->spi->write(buf, 5, NULL, 0);
    for (uint32_t i = 0; i < len; i++) {
	this->spi->write(&value, 1, NULL, 0);
    }
    deselect();
    spi->unlock();
}

void MB85RS4MT::zero(const uint32_t addr, size_t len) {
    fill(addr, len, 0);
}


void MB85RS4MT::select() {
    *chip_select = 0;
}

void MB85RS4MT::deselect() {
    *chip_select = 1;
}

void MB85RS4MT::insert_cmd_addr(char *buf, const char cmd, const uint32_t addr) {
    buf[0] = cmd;
    buf[1] = (char)(addr >> 16 & 0xff);
    buf[2] = (char)(addr >>  8 & 0xff);
    buf[3] = (char)(addr       & 0xff);
}

void MB85RS4MT::insert_cmd_addr_value(char *buf, const char cmd, const uint32_t addr, const char value) {
    insert_cmd_addr(buf, cmd, addr);
    buf[4] = value;
}

void MB85RS4MT::tx_cmd(const char cmd) {
    char buf[1] = {cmd};
    txrx(buf, 1, NULL, 0);
}

char MB85RS4MT::tx_get_value(const char *buf, const size_t len) {
    char value = 0;
    txrx(buf, len, &value, 1);
    return value;
}

void MB85RS4MT::txrx(const char *tx_buf, const size_t tx_len, char *rx_buf, const size_t rx_len) {
    spi->lock();
    select();
    if (tx_len)
	this->spi->write(tx_buf, tx_len, NULL, 0);
    if (rx_len)
	this->spi->write(NULL, 0, rx_buf, rx_len);
    deselect();
    spi->unlock();
}

