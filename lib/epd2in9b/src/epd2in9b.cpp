#include "epd2in9b.h"

namespace epd2in9b {

EPD2IN9B::EPD2IN9B(SPI* spi, const PinName cs_pin, const PinName dc_pin,
	const PinName busy_pin, const PinName reset_pin) {

    // Save args
    this->spi = spi;
    this->cs_pin = cs_pin;
    this->dc_pin = dc_pin;
    this->busy_pin = busy_pin;
    this->reset_pin = reset_pin;

    // Start GPIOs
    this->gpio_chip_select = new DigitalOut(cs_pin, 1);	    // 0 for select, 1 for release
    this->gpio_data_command = new DigitalOut(dc_pin, 0);    // 0 for command, 1 for data
    this->gpio_reset = new DigitalOut(reset_pin, 1);	    // 0 for reset, 1 for run
    this->gpio_busy = new DigitalIn(busy_pin);		    // 0 for busy, 1 for idle

    reset();
    clear();
    buf_black[5][5] = 0x00;
    buf_black[5][6] = 0x00;
    buf_black[5][7] = 0x00;
    buf_red[8][5] = 0x00;
    buf_red[8][6] = 0xff;
    buf_red[8][7] = 0x00;
    transfer();
    refresh();
    sleep();
}

// Moving data to the SRAM

void
EPD2IN9B::transfer() {
    transfer_black();
    transfer_red();
}

void
EPD2IN9B::transfer_black() {
    finish();
    select();
    send_command(EPD2IN9B_COMMAND_DATA_START_TRANSMISSION_1);           
    ThisThread::sleep_for(2ms);
    for (int h=0; h<EPD2IN9B_HEIGHT; h++) {
	for (int w=0; w<(EPD2IN9B_WIDTH/8); w++) {
	    send_data(buf_black[w][h]);
	}
    } 
    ThisThread::sleep_for(2ms);
    deselect();
}

void
EPD2IN9B::transfer_red() {
    finish();
    select();
    send_command(EPD2IN9B_COMMAND_DATA_START_TRANSMISSION_2);           
    ThisThread::sleep_for(2ms);
    for (int h=0; h<EPD2IN9B_HEIGHT; h++) {
	for (int w=0; w<(EPD2IN9B_WIDTH/8); w++) {
	    send_data(buf_red[w][h]);
	}
    } 
    ThisThread::sleep_for(2ms);
    deselect();
}

void
EPD2IN9B::transfer(const size_t pos_x, const size_t pos_y, const size_t width, const size_t length) {
    transfer_black(pos_x, pos_y, width, length);
    transfer_red(pos_x, pos_y, width, length);
}

void
EPD2IN9B::transfer_black(const size_t pos_x, const size_t pos_y, const size_t width, const size_t length) {
}

void
EPD2IN9B::transfer_red(const size_t pos_x, const size_t pos_y, const size_t width, const size_t length) {
}

void
EPD2IN9B::clear(void) {
    for (int h=0; h<EPD2IN9B_HEIGHT; h++) {
	for (int w=0; w<(EPD2IN9B_WIDTH/8); w++) {
	    buf_black[w][h] = 0xff;
	    buf_red[w][h] = 0xff;
	}
    } 
}

void EPD2IN9B::refresh(void) {
    finish();
    select();
    send_command(EPD2IN9B_COMMAND_DISPLAY_REFRESH);
    deselect();
}

void EPD2IN9B::reset(void) {
    gpio_reset->write(0);
    ThisThread::sleep_for(100ms);
    gpio_reset->write(1);
    ThisThread::sleep_for(100ms);

    select();
    send_command(EPD2IN9B_COMMAND_BOOSTER_SOFT_START);
    send_data(0x17);
    send_data(0x17);
    send_data(0x17);

    send_command(EPD2IN9B_COMMAND_POWER_ON);
    finish();

    send_command(EPD2IN9B_COMMAND_PANEL_SETTING);
    send_data(0x8F);

    send_command(EPD2IN9B_COMMAND_VCOM_AND_DATA_INTERVAL_SETTING);
    send_data(0x77);

    send_command(EPD2IN9B_COMMAND_TCON_RESOLUTION);
    send_data(0x80);
    send_data(0x01);
    send_data(0x28);

    send_command(EPD2IN9B_COMMAND_VCM_DC_SETTING_REGISTER);
    send_data(0x0A);
    deselect();
}

void
EPD2IN9B::send_command(const uint8_t command) {
    gpio_data_command->write(0);
    spi->write(command);
}

void
EPD2IN9B::send_data(const uint8_t data) {
    gpio_data_command->write(1);
    spi->write(data);
}

void
EPD2IN9B::finish(void) {
    while (!gpio_busy->read())
	ThisThread::sleep_for(10ms);
}

void
EPD2IN9B::sleep(void) {
    finish();
    select();
    send_command(EPD2IN9B_COMMAND_DEEP_SLEEP);
    send_data(0xa5);
    deselect();
}

void
EPD2IN9B::wake(void) {
    reset();
}

// SPI bus

void EPD2IN9B::spi_tx(const char *tx_buf, const size_t tx_len) {
    spi->write(tx_buf, tx_len, NULL, 0);
}

void EPD2IN9B::spi_tx(const int chr) {
    spi->write(chr);
}

void EPD2IN9B::select() {
    spi->lock();
    gpio_chip_select->write(0);
}

void EPD2IN9B::deselect() {
    gpio_chip_select->write(1);
    spi->unlock();
}

#if 0
    EPD2IN9B_COMMAND_PANEL_SETTING                  = 0x00,
    EPD2IN9B_COMMAND_POWER_SETTING                  = 0x01,
    EPD2IN9B_COMMAND_POWER_OFF                      = 0x02,
    EPD2IN9B_COMMAND_POWER_OFF_SEQUENCE_SETTING     = 0x03,
    EPD2IN9B_COMMAND_POWER_ON                       = 0x04,
    EPD2IN9B_COMMAND_POWER_ON_MEASURE               = 0x05,
    EPD2IN9B_COMMAND_BOOSTER_SOFT_START             = 0x06,
    EPD2IN9B_COMMAND_DEEP_SLEEP                     = 0x07,
    EPD2IN9B_COMMAND_DATA_START_TRANSMISSION_1      = 0x10,
    EPD2IN9B_COMMAND_DATA_STOP                      = 0x11,
    EPD2IN9B_COMMAND_DISPLAY_REFRESH                = 0x12,
    EPD2IN9B_COMMAND_DATA_START_TRANSMISSION_2      = 0x13,
    EPD2IN9B_COMMAND_PLL_CONTROL                    = 0x30,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_COMMAND     = 0x40,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_CALIBRATION = 0x41,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_WRITE       = 0x42,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_READ        = 0x43,
    EPD2IN9B_COMMAND_VCOM_AND_DATA_INTERVAL_SETTING = 0x50,
    EPD2IN9B_COMMAND_LOW_POWER_DETECTION            = 0x51,
    EPD2IN9B_COMMAND_TCON_SETTING                   = 0x60,
    EPD2IN9B_COMMAND_TCON_RESOLUTION                = 0x61,
    EPD2IN9B_COMMAND_GET_STATUS                     = 0x71,
    EPD2IN9B_COMMAND_AUTO_MEASURE_VCOM              = 0x80,
    EPD2IN9B_COMMAND_VCOM_VALUE                     = 0x81,
    EPD2IN9B_COMMAND_VCM_DC_SETTING_REGISTER        = 0x82,
    EPD2IN9B_COMMAND_PARTIAL_WINDOW                 = 0x90,
    EPD2IN9B_COMMAND_PARTIAL_IN                     = 0x91,
    EPD2IN9B_COMMAND_PARTIAL_OUT                    = 0x92,
    EPD2IN9B_COMMAND_PROGRAM_MODE                   = 0xA0,
    EPD2IN9B_COMMAND_ACTIVE_PROGRAM                 = 0xA1,
    EPD2IN9B_COMMAND_READ_OTP_DATA                  = 0xA2,
    EPD2IN9B_COMMAND_POWER_SAVING                   = 0xE3,
#endif

#if 0
/**
 *  @brief: transmit partial data to the SRAM
 */
void Epd::SetPartialWindow(const unsigned char* buffer_black, const unsigned char* buffer_red, int x, int y, int w, int l) {
    SendCommand(PARTIAL_IN);
    SendCommand(PARTIAL_WINDOW);
    SendData(x & 0xf8);     // x should be the multiple of 8, the last 3 bit will always be ignored
    SendData(((x & 0xf8) + w  - 1) | 0x07);
    SendData(y >> 8);        
    SendData(y & 0xff);
    SendData((y + l - 1) >> 8);        
    SendData((y + l - 1) & 0xff);
    SendData(0x01);         // Gates scan both inside and outside of the partial window. (default) 
    DelayMs(2);
    SendCommand(DATA_START_TRANSMISSION_1);
    if (buffer_black != NULL) {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(buffer_black[i]);  
        }  
    } else {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(0x00);  
        }  
    }
    DelayMs(2);
    SendCommand(DATA_START_TRANSMISSION_2);
    if (buffer_red != NULL) {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(buffer_red[i]);  
        }  
    } else {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(0x00);  
        }  
    }
    DelayMs(2);
    SendCommand(PARTIAL_OUT);  
}

/**
 *  @brief: transmit partial data to the black part of SRAM
 */
void Epd::SetPartialWindowBlack(const unsigned char* buffer_black, int x, int y, int w, int l) {
    SendCommand(PARTIAL_IN);
    SendCommand(PARTIAL_WINDOW);
    SendData(x & 0xf8);     // x should be the multiple of 8, the last 3 bit will always be ignored
    SendData(((x & 0xf8) + w  - 1) | 0x07);
    SendData(y >> 8);        
    SendData(y & 0xff);
    SendData((y + l - 1) >> 8);        
    SendData((y + l - 1) & 0xff);
    SendData(0x01);         // Gates scan both inside and outside of the partial window. (default) 
    DelayMs(2);
    SendCommand(DATA_START_TRANSMISSION_1);
    if (buffer_black != NULL) {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(buffer_black[i]);  
        }  
    } else {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(0x00);  
        }  
    }
    DelayMs(2);
    SendCommand(PARTIAL_OUT);  
}

/**
 *  @brief: transmit partial data to the red part of SRAM
 */
void Epd::SetPartialWindowRed(const unsigned char* buffer_red, int x, int y, int w, int l) {
    SendCommand(PARTIAL_IN);
    SendCommand(PARTIAL_WINDOW);
    SendData(x & 0xf8);     // x should be the multiple of 8, the last 3 bit will always be ignored
    SendData(((x & 0xf8) + w  - 1) | 0x07);
    SendData(y >> 8);        
    SendData(y & 0xff);
    SendData((y + l - 1) >> 8);        
    SendData((y + l - 1) & 0xff);
    SendData(0x01);         // Gates scan both inside and outside of the partial window. (default) 
    DelayMs(2);
    SendCommand(DATA_START_TRANSMISSION_2);
    if (buffer_red != NULL) {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(buffer_red[i]);  
        }  
    } else {
        for(int i = 0; i < w  / 8 * l; i++) {
            SendData(0x00);  
        }  
    }
    DelayMs(2);
    SendCommand(PARTIAL_OUT);  
}

/**
 * @brief: refresh and displays the frame
 */
void Epd::DisplayFrame(const unsigned char* frame_buffer_black, const unsigned char* frame_buffer_red) {
    if (frame_buffer_black != NULL) {
        SendCommand(DATA_START_TRANSMISSION_1);
        DelayMs(2);
        for (int i = 0; i < this->width * this->height / 8; i++) {
            SendData(pgm_read_byte(&frame_buffer_black[i]));
        }
        DelayMs(2);
    }
    if (frame_buffer_red != NULL) {
        SendCommand(DATA_START_TRANSMISSION_2);
        DelayMs(2);
        for (int i = 0; i < this->width * this->height / 8; i++) {
            SendData(pgm_read_byte(&frame_buffer_red[i]));
        }
        DelayMs(2);
    }
    SendCommand(DISPLAY_REFRESH);
    WaitUntilIdle();
}

/**
 * @brief: clear the frame data from the SRAM, this won't refresh the display
 */
void Epd::ClearFrame(void) {
    SendCommand(TCON_RESOLUTION);
    SendData(width >> 8);
    SendData(width & 0xff);
    SendData(height >> 8);        
    SendData(height & 0xff);

    SendCommand(DATA_START_TRANSMISSION_1);           
    DelayMs(2);
    for(int i = 0; i < width * height / 8; i++) {
        SendData(0xFF);  
    }  
    DelayMs(2);
    SendCommand(DATA_START_TRANSMISSION_2);           
    DelayMs(2);
    for(int i = 0; i < width * height / 8; i++) {
        SendData(0xFF);  
    }  
    DelayMs(2);
}

#endif

} // namespace epd2in9b
