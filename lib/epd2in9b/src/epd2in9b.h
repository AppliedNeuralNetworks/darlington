
#pragma once

#include <mbed.h>

// Display resolution
#define EPD2IN9B_WIDTH	(128)
#define EPD2IN9B_HEIGHT	(296)

namespace epd2in9b {

enum epd2in9b_commands {
    EPD2IN9B_COMMAND_PANEL_SETTING                  = 0x00,
    EPD2IN9B_COMMAND_POWER_SETTING                  = 0x01,
    EPD2IN9B_COMMAND_POWER_OFF                      = 0x02,
    EPD2IN9B_COMMAND_POWER_OFF_SEQUENCE_SETTING     = 0x03,
    EPD2IN9B_COMMAND_POWER_ON                       = 0x04,
    EPD2IN9B_COMMAND_POWER_ON_MEASURE               = 0x05,
    EPD2IN9B_COMMAND_BOOSTER_SOFT_START             = 0x06,
    EPD2IN9B_COMMAND_DEEP_SLEEP                     = 0x07,
    EPD2IN9B_COMMAND_DATA_START_TRANSMISSION_1      = 0x10,
    EPD2IN9B_COMMAND_DATA_STOP                      = 0x11,
    EPD2IN9B_COMMAND_DISPLAY_REFRESH                = 0x12,
    EPD2IN9B_COMMAND_DATA_START_TRANSMISSION_2      = 0x13,
    EPD2IN9B_COMMAND_PLL_CONTROL                    = 0x30,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_COMMAND     = 0x40,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_CALIBRATION = 0x41,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_WRITE       = 0x42,
    EPD2IN9B_COMMAND_TEMPERATURE_SENSOR_READ        = 0x43,
    EPD2IN9B_COMMAND_VCOM_AND_DATA_INTERVAL_SETTING = 0x50,
    EPD2IN9B_COMMAND_LOW_POWER_DETECTION            = 0x51,
    EPD2IN9B_COMMAND_TCON_SETTING                   = 0x60,
    EPD2IN9B_COMMAND_TCON_RESOLUTION                = 0x61,
    EPD2IN9B_COMMAND_GET_STATUS                     = 0x71,
    EPD2IN9B_COMMAND_AUTO_MEASURE_VCOM              = 0x80,
    EPD2IN9B_COMMAND_VCOM_VALUE                     = 0x81,
    EPD2IN9B_COMMAND_VCM_DC_SETTING_REGISTER        = 0x82,
    EPD2IN9B_COMMAND_PARTIAL_WINDOW                 = 0x90,
    EPD2IN9B_COMMAND_PARTIAL_IN                     = 0x91,
    EPD2IN9B_COMMAND_PARTIAL_OUT                    = 0x92,
    EPD2IN9B_COMMAND_PROGRAM_MODE                   = 0xA0,
    EPD2IN9B_COMMAND_ACTIVE_PROGRAM                 = 0xA1,
    EPD2IN9B_COMMAND_READ_OTP_DATA                  = 0xA2,
    EPD2IN9B_COMMAND_POWER_SAVING                   = 0xE3,
};

class EPD2IN9B {
    public:

	// Back buffers for each color
	char buf_black[EPD2IN9B_WIDTH][EPD2IN9B_HEIGHT] = {0xff};
	char buf_red[EPD2IN9B_WIDTH][EPD2IN9B_HEIGHT] = {0xff};

	/* Constructor
	 *  Enables write on construction
	 *
	 *  spi: Pointer to an mbed::SPI instance. Harmless if SPI was instantiated with an SSEL pin,
	 *	since exclusive access here uses lock() instead of select(), and does its own chip
	 *	select by explicitly toggling the pin named by the cs_pin parameter, below.
	 *  cs_pin: mbed::PinName of pin connected to the peripheral's chip select pin
	 *  dc_pin: mbed::PinName of pin connected to the peripheral's data/command pin
	 *  busy_pin: mbed::PinName of pin connected to the peripheral's busy_pin
	 *  reset_pin: mbed::PinName of pin connected to the peripheral's reset_pin
	 */
	EPD2IN9B(SPI* spi, const PinName cs_pin, const PinName dc_pin, const PinName busy_pin,
		const PinName reset_pin);

	/* Destructor */
	~EPD2IN9B();

	void send_command(const uint8_t command);
	void send_data(const uint8_t data);

	void clear(void);
	void refresh(void);
	void transfer();
	void transfer_red();
	void transfer_black();
	void transfer(const size_t pos_x, const size_t pos_y, const size_t width, const size_t length);
	void transfer_black(const size_t pos_x, const size_t pos_y, const size_t width, const size_t length);
	void transfer_red(const size_t pos_x, const size_t pos_y, const size_t width, const size_t length);
	void finish(void);

    private:
	// Args
        SPI* spi = NULL;
	uint8_t cs_pin = 0;
	uint8_t dc_pin = 0;
	uint8_t reset_pin = 0;
	uint8_t busy_pin = 0;

	// I/O
        DigitalOut* gpio_chip_select = NULL;
        DigitalOut* gpio_data_command = NULL;
        DigitalOut* gpio_reset = NULL;
        DigitalIn*  gpio_busy = NULL;

	void sleep(void);
	void wake(void);
	void reset(void);

	void spi_tx(const char *tx_buf, const size_t tx_len);
	void spi_tx(const int chr);
	void select(void);
	void deselect(void);
};

} // namespace epd2in9b
