#pragma once
#include <mbed.h>

namespace cap1188 {

enum cap1188_registers {
    CAP1188_MAIN = 0x00,
    CAP1188_MAIN_INT = 0x01,
    CAP1188_SENINPUTSTATUS = 0x3,
    CAP1188_MTBLK = 0x2A,
    CAP1188_STANDBYCFG = 0x41,
    CAP1188_CONFIG2 = 0x44,
    CAP1188_LEDLINK = 0x72,
    CAP1188_LEDPOL = 0x73,
    CAP1188_LED_BEHAVIOR1 = 0x81,
    CAP1188_LED_BEHAVIOR2 = 0x82,
    CAP1188_LED_CONFIG = 0x88,
    CAP1188_LED_OFF_DELAY = 0x95,
    CAP1188_PRODID = 0xFD,
    CAP1188_MANUID = 0xFE,
    CAP1188_REV = 0xFF,
};

class CAP1188 {
    public:
	/* Object Lifecycle
	 *
	 *  i2c: Pointer to an mbed::I2C instance
	 *  addr: I2C address of device (default: 0x29)
	 */
	CAP1188(I2C *i2c, const uint8_t address=0x29);
	~CAP1188();


    private:
	// Args
        I2C *i2c = NULL;
	uint8_t address = 0;

	int tx_regptr(const char reg);
	int tx_reg(const char reg, const char value);
	int rx_reg(const char reg, char *out);
};

} // namespace cap1188
