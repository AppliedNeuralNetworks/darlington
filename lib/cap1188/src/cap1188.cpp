#include <cstdarg>
#include "cap1188.h"

using namespace std::chrono;

namespace cap1188 {

CAP1188::CAP1188(I2C *i2c, const uint8_t address) {
    // Save args
    this->i2c = i2c;
    this->address = address << 1;

    // Configure the chip
    tx_reg(CAP1188_MTBLK,	    0x00); // Allow multiple touches
    tx_reg(CAP1188_LEDLINK,	    0xff); // Enable LED response
    tx_reg(CAP1188_LED_OFF_DELAY,   0x81); // LED slow fade
    tx_reg(0x28,   0x00);
    tx_reg(0x44,   0x41);
    tx_reg(0x41,   0x39);
}

CAP1188::~CAP1188() {
}

int
CAP1188::tx_regptr(const char reg) {
    return i2c->write(address, &reg, 1);
}

int
CAP1188::tx_reg(const char reg, const char value) {
    char buf[2] = { reg, value };
    return i2c->write(address, buf, 2);
}

int
CAP1188::rx_reg(const char reg, char *out) {
    if (tx_regptr(reg))
	return -1;
    return i2c->read(address, out, 1);
}

}; // namespace cap1188

