#include "mt3339.h"

namespace mt3339 {

// Extracted Data
static struct gps_location_t gps_location;
static struct gps_time_t gps_time;


// Message Processing (Internal)
Mutex time_mutex, location_mutex;
static int8_t hex_digit_to_dec(const char digit);
static uint8_t compute_checksum(const char* msg);
static bool verify_checksum(const char* msg);
uint32_t id_hash(char *id);
DigitalOut led2(LED2);


// Inter-thread communication
typedef struct {
    char contents[GPS_MAX_MESSAGE_LENGTH];
} letter_t;
Mail<letter_t, 8> mailbox;


// Instantiation and Dispensation
// TODO: MT3339::~MT3339() {}
MT3339::MT3339(BufferedSerial *gps_serial) {
    serial = gps_serial;
    //gps_serial.write("$PMTK220,1000*1F\r\n"); // 1000ms updates
    set_nmea_output(GPS_MSG_ENABLE_RMC|GPS_MSG_ENABLE_GGA);

    thread_get_mail = new Thread();
    thread_get_serial = new Thread();
}


// --- Service Control ---
// TODO: void MT3339::stop() {}
void
MT3339::start() {
    thread_get_mail->start(callback(get_mail));
    thread_get_serial->start(callback(get_serial, serial));
}


// --- Device Control ---

void
MT3339::set_baud(const unsigned int rate) {
    char buf[15];
    sprintf(buf, "$PMTK251,%u", rate);
    send_command(buf);
    // CRASH!: This locks the system.
    // serial->set_baud(rate);
}

void
MT3339::send_command(const char* msg) {
    char buf[6];					// sprintf will append null
    sprintf(buf, "*%02hhX\r\n", compute_checksum(msg));
    serial->write(msg, strlen(msg));
    serial->write(buf, 5);				// skip the null
}

void
MT3339::set_nmea_output(uint8_t types_bitmap) {
    // Field positions:
    //	0: GLL (Geographic Position)
    //	1: RMC (Recommended Minimum Data)
    //	2: VTG (Course over Ground, Ground Speed)
    //	3: GGA (GPS Fix Info)
    //	4: GSA (Active Satellites)
    //	5: GSV (Satellites in View)
    // 17: ZDA (Time and Date)
    //
    // Field Values:
    //	0: Disable this sentence type
    //	1: Output once per 1 fix
    //	 : ...
    //	5: Output once per 5 fixes
    char buf[54] = {0};
    strcpy(buf, "$PMTK314");
#   define IS_SELECTED(type) \
	(uint8_t)(GPS_MSG_ENABLE_##type&types_bitmap ? 1 : 0)
    uint8_t options[] = {
	IS_SELECTED(GLL), IS_SELECTED(RMC), IS_SELECTED(VTG),
	IS_SELECTED(GGA), IS_SELECTED(GSA), IS_SELECTED(GSV),
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	IS_SELECTED(ZDA),
	0, 0
    };
    for (uint8_t i = 0; i < 19; i++) 
	sprintf(buf + 8 + i*2, ",%d", options[i]);
    send_command(buf);
}

// --- Data Retrieval ---

bool
MT3339::get_time(struct gps_time_t *ptr) {
    time_mutex.lock();
    if (!gps_time.valid) {
	time_mutex.unlock();
	return false;
    }
    memcpy(ptr, &gps_time, sizeof(gps_time_t));
    time_mutex.unlock();
    return true;
}

bool
MT3339::get_time_str(char *ptr) {
    struct gps_time_t now;
    if (!get_time(&now)) {
	return false;
    }
    sprintf(ptr, "%d-%02d-%02d %02d:%02d:%02d",
	now.year, now.month,  now.day,
	now.hour, now.minute, (int)now.second); 
    return true;
}

bool
MT3339::get_location(struct gps_location_t *ptr) {
    location_mutex.lock();
    if (!gps_location.valid) {
	location_mutex.unlock();
	return false;
    }
    memcpy(ptr, &gps_location, sizeof(gps_location_t));
    location_mutex.unlock();
    return true;
}

bool
MT3339::get_location_str(char *ptr) {
    struct gps_location_t here;
    if (!get_location(&here)) {
	return false;
    }
    sprintf(ptr, "%02d%c%02.3f' %03d%c%02.3f' %.3f",
	here.lat_degrees, *here.n_s, here.lat_minutes,
	here.lon_degrees, *here.e_w, here.lon_minutes,
	here.altitude); 
    return true;
}


// --- Message Processing ---
void
MT3339::get_serial(BufferedSerial *gps_serial) {
    char c, buf[GPS_MAX_MESSAGE_LENGTH] = {0};
    size_t bufpos = 0;

    for (;;) {
	gps_serial->read(&c, 1);
	if (bufpos >= GPS_MAX_MESSAGE_LENGTH) {	// re-sync if runaway
	    bufpos = 0;
	    while (c != '$')
		gps_serial->read(&c, 1);
	}
	if (c == '\r')				// discard carriage return
	    continue;
	if (c == '\n') {			// trigger processing
	    buf[bufpos] = 0;			// end string with null
	    if (buf[0] == '$'			// must begin with '$'
		    and buf[bufpos - 3] == '*'	// must have '*' at end-3
		    and verify_checksum(buf)) {
		if (!mailbox.full())		// Send mail
		    send_mail(buf);
	    }
	    bufpos = 0;				// reset buffer and counter
	    memset(buf, 0, GPS_MAX_MESSAGE_LENGTH);
	    continue;
	}
	buf[bufpos++] = c;			// normal char, append
    }
}

bool
MT3339::send_mail(char *message) {
    letter_t *letter = mailbox.try_alloc();
    if (!letter)
	return false;
    strncpy(letter->contents, message, GPS_MAX_MESSAGE_LENGTH);
    mailbox.put(letter);

    return true;
}

void
MT3339::get_mail() {
    for (;;) {
	letter_t *letter = mailbox.try_get_for(Kernel::wait_for_u32_forever);
	led2 = 1;
	switch (id_hash(letter->contents)) {
	    case 4671297:
		parse_gpgga(letter->contents);
		break;
	    case 5393731:
		parse_gprmc(letter->contents);
		break;
	}
	mailbox.free(letter);
	led2 = 0;
    }
}

uint8_t
compute_checksum(const char* msg) {
    uint8_t checksum = 0;
    size_t pos = 1;				    // Start after $
    while (msg[pos] != '*' and msg[pos] != '\0')
	checksum ^= msg[pos++];			    // XOR all chars

    return checksum;
}

bool
verify_checksum(const char* msg) {
    size_t pos = 0;
    while (msg[pos++] != '*');
    uint8_t checksum_from_message = hex_digit_to_dec(msg[pos]) << 4
	| hex_digit_to_dec(msg[pos+1]);

    return checksum_from_message == compute_checksum(msg);
}

int8_t
hex_digit_to_dec(const char digit) {
    static char lut[] = "0123456789ABCDEF0000000000abcdef";
    for (size_t i=0; i<32; i++)
	if (lut[i] == digit)
	   return (i & ~16);

    return -1; 
}

uint32_t
id_hash(char *id) {
    return (uint32_t)id[3] << 16 | (uint32_t)id[4] << 8 | id[5];
}

uint8_t
MT3339::extract_fields(char *message, char fields[][GPS_MAX_FIELD_LENGTH]) {
    memset(fields, 0, GPS_MAX_NUM_FIELDS * GPS_MAX_FIELD_LENGTH);
    uint8_t field_num = 0, fpos = 0;
    char *mpos = message + 1;			// Start search after "$"

    while (*mpos != '*') {			// go to end of fields "*"
	if (*mpos == ',') {			// comma separates fields
	    ++mpos;				// move past comma
	    ++field_num; fpos = 0;		// start at next field
	}
	else
	    fields[field_num][fpos++] = *mpos++;// next position in field and
    }						// ...next position in message
    return field_num;
}

void
MT3339::set_rtc() {
    if (!gps_time.valid)
	return;

    struct tm timebuf = {
	(int)gps_time.second,
	gps_time.minute,
	gps_time.hour,
	gps_time.day,
	gps_time.month,
	gps_time.year,
	0
    };
    set_time(mktime(&timebuf));
}


// NMEA Sentence Processing

void
MT3339::parse_gprmc(char *message) {
    /*
    RMC: Minimum Recommended Sentence C
    Position, Velocity, and Time

    0	   1	      2 3	  4 5	       6 7    8      9	   10 11
    |	   |	      | |	  | |	       | |    |      |	    | |
    $--RMC,hhmmss.sss,A,ddmm.mmmm,N,ddmmm.mmmm,E,v.vv,ddd.dd,ddmmyy,M*10

    0: Preamble
       Always begins with "$", then
       Talker (GP = GPS, GL = GLONASS, GA = Galileo, BD/GB = Beidou), two chars
       Message type (RMC = Recommended Minimum Sentence C), three chars
    1: Time (UTC)
    2: Status (A = Data Valid, V = Data Not Valid)
    3: Latitude
    4: N or S (North or South)
    5: Longitude
    6: E or W (East or West)
    7: Speed over Ground (knots)
    8: Course over Ground (degrees)
    9: Date
    10: Mode (A = Autonomous, D = DGPS, E = Dead Reckoning)
    11: Checksum (hex)
    */
    char fields[GPS_MAX_NUM_FIELDS][GPS_MAX_FIELD_LENGTH];
    extract_fields(message, fields);

    char buf[7];

    time_mutex.lock();

    char *dmy = fields[9];
    strlcpy(buf, dmy, 3);
    gps_time.day = atoi(buf);
    strlcpy(buf, dmy+2, 3);
    gps_time.month = atoi(buf);
    strlcpy(buf, dmy+4, 3);
    gps_time.year = atoi(buf) + 2000;  // Y2K

    char *hms = fields[1];
    strlcpy(buf, hms, 3);
    gps_time.hour = atoi(buf);
    strlcpy(buf, hms+2, 3);
    gps_time.minute = atoi(buf);
    strlcpy(buf, hms+4, 3);
    gps_time.second = atoi(buf);
    gps_time.valid = true;

    // Set RTC at the top of the hour, or anytime the RTC is unset.
    if ((gps_time.minute == 0 && gps_time.second == 0) || time(NULL) == 0)
	set_rtc();

    time_mutex.unlock();
}

void
MT3339::parse_gpgga(char *message) {
    /*
    GGA: Global Positioning System Fix Data:
    Time, Position and fix related data for a GPS receiver

    0	   1	      2		3 4	   5 6 7  8   9  10 11 12 13  14   15
    |	   |	      |		| |	   | | |  |   |   | |	| |   |    |
    $--GGA,hhmmss.sss,ddmm.mmmm,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh

    0: Preamble
       Always begins with "$", then
       Talker (GP = GPS, GL = GLONASS, GA = Galileo, BD/GB = Beidou), two chars
       Message type (GGA = GPS fix), three chars
    1: Time (UTC)
    2: Latitude
    3: N or S (North or South)
    4: Longitude
    5: E or W (East or West)
    6: GPS Quality Indicator,
	   0: Fix not available or invalid
	   1: GPS SPS Mode, fix valid
	   2: Differential GPS, SPS Mode, fix valid
	   3-5: Not supported
	   6: Dead Reckoning Mode, fix valid
    7: Number of satellites in view, 00 - 12
    8: Horizontal Dilution of precision
    9: Antenna Altitude above/below mean-sea-level (geoid)
    10: Units of antenna altitude, meters
    11: Geoidal separation, the difference between the WGS-84
	earth ellipsoid and mean-sea-level (geoid), "-" means
	mean-sea-level below ellipsoid
    12: Units of geoidal separation, meters
    13: Age of differential GPS data, time in seconds since
	last SC104 type 1 or 9 update, null field when DGPS is not used
    14: Differential reference station ID, 0000-1023
    15: Checksum (hex)
    */
    char fields[GPS_MAX_NUM_FIELDS][GPS_MAX_FIELD_LENGTH];
    extract_fields(message, fields);

    if (fields[6][0] == '0')  // No GPS fix yet, don't update values.
	return;

    location_mutex.lock();

    // 2. Latitude (ddmm.mmmm) and 3. Latitude Hemisphere (N/S)
    double latitude = strtod(fields[2], NULL);
    double lat_degrees;
    double lat_minutes = modf(latitude / 100, &lat_degrees) * 100; 
    gps_location.lat_degrees = lat_degrees;
    gps_location.lat_minutes = lat_minutes;
    *gps_location.n_s = fields[3][0];

    // 4. Longitude (dddmm.mmmm) and 5. Longitude Hemisphere (E/W)
    double longitude = strtod(fields[4], NULL);
    double lon_degrees;
    double lon_minutes = modf(longitude / 100, &lon_degrees) * 100; 
    gps_location.lon_degrees = lon_degrees;
    gps_location.lon_minutes = lon_minutes;
    *gps_location.e_w = fields[5][0];

    // 9. Mean-sea-level Altitude
    gps_location.altitude = strtod(fields[9], NULL);
    gps_location.valid = true;

    location_mutex.unlock();
}

}  // namespace mt3339
