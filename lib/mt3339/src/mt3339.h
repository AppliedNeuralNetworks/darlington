#pragma once

#include <mbed.h>

#ifndef GPS_MAX_MESSAGE_LENGTH
#    define GPS_MAX_MESSAGE_LENGTH  (82)
#endif
#ifndef GPS_MAX_NUM_FIELDS
#    define GPS_MAX_NUM_FIELDS	    (20)
#endif
#ifndef GPS_MAX_FIELD_LENGTH
#    define GPS_MAX_FIELD_LENGTH    (16)
#endif

namespace mt3339 {

enum pa1616_msg_type_enable {
    GPS_MSG_ENABLE_GLL = 0x01,
    GPS_MSG_ENABLE_RMC = 0x02,
    GPS_MSG_ENABLE_VTG = 0x04,
    GPS_MSG_ENABLE_GGA = 0x08,
    GPS_MSG_ENABLE_GSA = 0x10,
    GPS_MSG_ENABLE_GSV = 0x20,
    GPS_MSG_ENABLE_ZDA = 0x40,
};

struct gps_time_t {
    volatile uint16_t year;
    volatile uint8_t month;
    volatile uint8_t day;
    volatile uint8_t hour;
    volatile uint8_t minute;
    volatile float second;

    volatile bool valid;
};

struct gps_location_t {
    volatile int8_t lat_degrees;
    volatile double lat_minutes;
    volatile char n_s[1];
    volatile int8_t lon_degrees;
    volatile double lon_minutes;
    volatile char e_w[1];
    volatile double altitude;

    volatile bool valid;
};

class MT3339 {
    public:
	// Instantiation and Dispensation
	MT3339(BufferedSerial *gps_serial);
	//~MT3339();

	// Service Control
	void start();
	void stop();

	// Data Retrieval
	bool get_time(struct gps_time_t *ptr);
	bool get_time_str(char *ptr);
	bool get_location(struct gps_location_t *ptr);
	bool get_location_str(char *ptr);

	// --- Device Control ---
	void send_command(const char* msg);
	void set_nmea_output(uint8_t types_bitmap);
	void set_baud(const unsigned int rate);


    private:
	// Communication
	BufferedSerial *serial = NULL;

	// Message Acquirey
	Thread* thread_get_serial;
	Thread* thread_get_mail;
	static void get_serial(BufferedSerial *serial);
	static void get_mail();
	static bool send_mail(char *message);

	// Message Processing
	void process_message();
	static uint8_t extract_fields(char *message,
		char fields[][GPS_MAX_FIELD_LENGTH]);
	static void parse_gpgga(char *message);
	static void parse_gprmc(char *message);
	static void set_rtc();
};

}  // namspace mt3339
