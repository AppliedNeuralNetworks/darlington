
#include <mbed.h>

#pragma once

#define MCP23017_PORTA_IODIR	0x00 // I/O direction
#define MCP23017_PORTA_IPOL	0x02 // Input polarity
#define MCP23017_PORTA_GPINTEN	0x04 // Interrupt on change
#define MCP23017_PORTA_DEFVAL	0x06 // Default compare val for IOC
#define MCP23017_PORTA_INTCON	0x08 // Interrupt control
#define MCP23017_PORTA_IOCON	0x0A // Configuration
#define MCP23017_PORTA_GPPU	0x0C // GPIO pull-ups
#define MCP23017_PORTA_INTF	0x0E // Interrupt flags
#define MCP23017_PORTA_INTCAP	0x10 // Interrupt captured value
#define MCP23017_PORTA_GPIO	0x12 // GPIO values
#define MCP23017_PORTA_OLAT	0x14 // Output latches

#define MCP23017_PORTB_IODIR	0x01
#define MCP23017_PORTB_IPOL	0x03
#define MCP23017_PORTB_GPINTEN	0x05
#define MCP23017_PORTB_DEFVAL	0x07
#define MCP23017_PORTB_INTCON	0x09
#define MCP23017_PORTB_IOCON	0x0B
#define MCP23017_PORTB_GPPU	0x0D
#define MCP23017_PORTB_INTF	0x0F
#define MCP23017_PORTB_INTCAP	0x11
#define MCP23017_PORTB_GPIO	0x13
#define MCP23017_PORTB_OLAT	0x15

#ifndef MCP23017_ADDR
#define MCP23017_ADDR (0x27)
#endif

class MCP23017 {
    public:
	MCP23017(I2C* i2c, const int addr);

	void write(const uint8_t reg, const uint8_t val);

    private:
	I2C* i2c = NULL;
	int addr = 0;
};
