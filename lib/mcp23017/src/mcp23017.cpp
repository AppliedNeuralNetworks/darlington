
#include "mcp23017.h"

MCP23017::MCP23017(I2C* i2c, const int addr) {
    this->i2c = i2c;
    this->addr = addr;
}

void MCP23017::write(uint8_t reg, uint8_t val) {
    char cmd[2];
    cmd[0] = reg;
    cmd[1] = val;
    while (this->i2c->write(MCP23017_ADDR << 1, cmd, 2) == -1);
}

