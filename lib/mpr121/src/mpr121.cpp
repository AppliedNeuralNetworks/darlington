#include "mpr121.h"

namespace mpr121 {

MPR121::MPR121(I2C *i2c, const uint8_t address) {
    // Save args
    this->i2c = i2c;
    this->address = address << 1;
    //char dt = 0x5d;
    //for (int i = 0x5a; i <= 0x5d; i++) {
	//printf("Trying %d: %d\n", i, i2c->read(i, &dt, 1));
	//printf("Trying %d: %d\n", i<<1, i2c->read(i<<1, &dt, 1));
    //}

    setup();
}

MPR121::~MPR121() {
}

uint16_t MPR121::get() {
    char buf[2] = {0};
    rx(0, buf, 2);
    return (buf[0] << 8 | buf[1]);
}

int
MPR121::setup() {
    // Filtering: data > baseline
    tx_reg(MPR121_MHD_R, 0x01);
    tx_reg(MPR121_NHD_R, 0x01);
    tx_reg(MPR121_NCL_R, 0x00);
    tx_reg(MPR121_FDL_R, 0x00);
    
    // Filtering: data < baseline
    tx_reg(MPR121_MHD_F, 0x01);
    tx_reg(MPR121_NHD_F, 0x01);
    tx_reg(MPR121_NCL_F, 0xFF);
    tx_reg(MPR121_FDL_F, 0x02);
    
    // Per-electrode touch/release thresholds
    tx_reg(MPR121_ELE0_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE0_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE1_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE1_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE2_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE2_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE3_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE3_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE4_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE4_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE5_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE5_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE6_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE6_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE7_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE7_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE8_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE8_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE9_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE9_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE10_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE10_R, MPR121_REL_THRESH);
    tx_reg(MPR121_ELE11_T, MPR121_TOU_THRESH);
    tx_reg(MPR121_ELE11_R, MPR121_REL_THRESH);
    
    // Filter configuration: Set ESI2
    tx_reg(MPR121_FIL_CFG, 0x04);
    
    // Enable auto-config and auto-reconfig
    tx_reg(MPR121_ATO_CFG0, 0x1B);
    tx_reg(MPR121_ATO_CFGU, 0xC9);	// USL = (Vdd-0.7)/vdd*256 = 0xC9 @3.3V
    tx_reg(MPR121_ATO_CFGL, 0x82);	// LSL = 0.65*USL = 0x82 @3.3V
    tx_reg(MPR121_ATO_CFGT, 0xB5);	// Target = 0.9*USL = 0xB5 @3.3V
    
    // Enable Electrodes (set to 0 for standby)
    tx_reg(MPR121_ELE_CFG, 12);		// Enable all 12 electrodes

    return 0;
}

int
MPR121::tx_regptr(const char reg) {
    i2c->lock();
    int result = i2c->write(address, &reg, 1);
    i2c->unlock();
    return result;
}

int
MPR121::tx_reg(const char reg, const char value) {
    char buf[2] = { reg, value };

    i2c->lock();
    int result = i2c->write(address, buf, 2);
    i2c->unlock();

    return result;
}

int
MPR121::rx(const char reg, char *out, size_t len) {
    int result = 0;

    i2c->lock();
    result = i2c->write(address, &reg, 1);
    if (result)
	return result;

    result = i2c->read(address, out, len);
    i2c->unlock();

    return 0;
}

}; // namespace mpr121

