#pragma once
#include <mbed.h>

#ifndef MPR121_I2C_ADDRESS
#define MPR121_I2C_ADDRESS (0x5a)
#endif

namespace mpr121 {

enum mpr121_registers {
    MPR121_MHD_R	= 0x2B,
    MPR121_NHD_R	= 0x2C,
    MPR121_NCL_R 	= 0x2D,
    MPR121_FDL_R	= 0x2E,
    MPR121_MHD_F	= 0x2F,
    MPR121_NHD_F	= 0x30,
    MPR121_NCL_F	= 0x31,
    MPR121_FDL_F	= 0x32,
    MPR121_ELE0_T	= 0x41,
    MPR121_ELE0_R	= 0x42,
    MPR121_ELE1_T	= 0x43,
    MPR121_ELE1_R	= 0x44,
    MPR121_ELE2_T	= 0x45,
    MPR121_ELE2_R	= 0x46,
    MPR121_ELE3_T	= 0x47,
    MPR121_ELE3_R	= 0x48,
    MPR121_ELE4_T	= 0x49,
    MPR121_ELE4_R	= 0x4A,
    MPR121_ELE5_T	= 0x4B,
    MPR121_ELE5_R	= 0x4C,
    MPR121_ELE6_T	= 0x4D,
    MPR121_ELE6_R	= 0x4E,
    MPR121_ELE7_T	= 0x4F,
    MPR121_ELE7_R	= 0x50,
    MPR121_ELE8_T	= 0x51,
    MPR121_ELE8_R	= 0x52,
    MPR121_ELE9_T	= 0x53,
    MPR121_ELE9_R	= 0x54,
    MPR121_ELE10_T	= 0x55,
    MPR121_ELE10_R	= 0x56,
    MPR121_ELE11_T	= 0x57,
    MPR121_ELE11_R	= 0x58,
    MPR121_FIL_CFG	= 0x5D,
    MPR121_ELE_CFG	= 0x5E,
    MPR121_GPIO_CTRL0	= 0x73,
    MPR121_GPIO_CTRL1	= 0x74,
    MPR121_GPIO_DATA	= 0x75,
    MPR121_GPIO_DIR	= 0x76,
    MPR121_GPIO_EN	= 0x77,
    MPR121_GPIO_SET	= 0x78,
    MPR121_GPIO_CLEAR	= 0x79,
    MPR121_GPIO_TOGGLE	= 0x7A,
    MPR121_ATO_CFG0	= 0x7B,
    MPR121_ATO_CFGU	= 0x7D,
    MPR121_ATO_CFGL	= 0x7E,
    MPR121_ATO_CFGT	= 0x7F,
};

enum mpr121_constants {
    MPR121_TOU_THRESH	= 0x0F,
    MPR121_REL_THRESH	= 0x0A,
};

class MPR121 {
    public:
	/* Object Lifecycle
	 *
	 *  i2c: Pointer to an mbed::I2C instance
	 *  addr: I2C address of device (default: 0x29)
	 */
	MPR121(I2C *i2c, const uint8_t address=MPR121_I2C_ADDRESS);
	~MPR121();

	uint16_t get();
	int tx_regptr(const char reg);
	int tx_reg(const char reg, const char value);
	int rx(const char reg, char *out, size_t len);

    private:
	// Args
        I2C *i2c = NULL;
	uint8_t address = 0;

	int setup();

};

} // namespace mpr121
